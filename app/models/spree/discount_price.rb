class Spree::DiscountPrice < ActiveRecord::Base
  belongs_to :price, :class_name => "Spree::Price"
  has_one :calculator, :class_name => "Spree::Calculator", :as => :calculable, :dependent => :destroy
  accepts_nested_attributes_for :calculator
  validates :calculator, :presence => true

  attr_accessible :amount, :discount_type, :store_only, :enabled, :start_at, :end_at, :price_id

  scope :active, lambda {
    where("enabled = 't' AND (start_at <= ? OR start_at IS NULL) AND (end_at >= ? OR end_at IS NULL)", Time.now, Time.now)
  }

  def calculator_type
    calculator.class.to_s if calculator
  end

  def calculator_type=(calculator_type)
    klass = calculator_type.constantize if calculator_type
    self.calculator = klass.new if klass and not self.calculator.is_a? klass
  end

  def price
    calculator.compute self
  end

  def discount_percentage
    amount
  end

  def enable
    update_attribute(:enabled, true)
  end

  def disable
    update_attribute(:enabled, false)
  end

  def start(end_time = nil)
    #if end_time is not in the future then make it nil (no end)
    end_time = nil if end_time.present? && end_time <= Time.now
    attr = { end_at: end_time, enabled: true }
    #only set start_at if it 's not set in the past
    attr[:start_at] = Time.now if self.start_at.present? && self.start_at > Time.now
    update_attributes(attr)
  end

  def stop
    update_attributes({ end_at: Time.now, enabled: false})
  end
end
