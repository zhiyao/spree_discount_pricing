Spree::Price.class_eval do
  has_one :discount_price

  def put_on_sale(value, calculator_type = "Spree::Calculator::PercentOffSalePriceCalculator", start_at = Time.now, end_at = nil, enabled = true)
    new_sale(value, calculator_type, start_at, end_at, enabled).save
  end
  alias :create_sale :put_on_sale

  def new_sale(value, calculator_type, start_at = Time.now, end_at = nil, enabled = true)
    new_discount_price = Spree::DiscountPrice.where(price_id: self[:id]).first_or_create()
    new_discount_price.update_attributes(amount: value, start_at: start_at, end_at: end_at, enabled: enabled)
    new_discount_price.calculator_type = calculator_type
    new_discount_price
  end

  def active_sale
    #on_sale? ? first_sale(discount_prices.active) : nil
    first_sale
  end
  alias :current_sale :active_sale

  def next_active_sale
    #discount_prices.present? ? first_sale(discount_prices) : nil
    first_sale
  end
  alias :next_current_sale :next_active_sale

  def discount_price
    on_sale? ? active_sale.amount : nil
  end

  def discount_price=(value)
    on_sale? ? active_sale.update_attribute(:amount, value) : put_on_sale(value)
  end

  def discount_percent
    on_sale? ? (1 - (discount_price / original_price)) * 100 : 0.0
  end

  def on_sale?
    #discount_prices.active.present? && first_sale(discount_prices.active).amount != original_price
    is_master_on_sale? && !first_sale.nil? && (first_sale.enabled && valid_start_at(first_sale.start_at) && valid_end_at(first_sale.end_at)) && first_sale.amount != original_price
  end

  def is_master_on_sale?
    if variant.nil?
      false
    elsif variant.is_master
      true
    elsif variant.product.master.nil?
      false
    else
      variant.product.price_in(Spree::Config[:currency]).on_sale?
    end
  end

  def valid_start_at(date)
    date.nil? || date < Date.today
  end

  def valid_end_at(date)
    date.nil? || date > Date.today
  end

  def original_price
    self[:amount]
  end

  def original_price=(value)
    self.price = value
  end

  def price
    on_sale? ? discount_price : original_price
  end

  def amount
    price
  end

  def enable_sale
    return nil unless next_active_sale.present?
    next_active_sale.enable
  end

  def disable_sale
    return nil unless active_sale.present?
    active_sale.disable
  end

  def start_sale(end_time = nil)
    return nil unless next_active_sale.present?
    next_active_sale.start(end_time)
  end

  def stop_sale
    return nil unless active_sale.present?
    active_sale.stop
  end

  def first_sale
    #scope.order("created_at DESC").first
    Spree::DiscountPrice.where(price_id: self[:id]).first
  end
end
