module Spree
  class Calculator::DollarAmountSalePriceCalculator < Spree::Calculator
    def self.description
      "Calculates the sale price for a Variant by returning the provided fixed sale price"
    end

    def compute(sale_price)
      sale_price.amount
    end
  end
end
