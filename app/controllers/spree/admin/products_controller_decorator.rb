Spree::Admin::ProductsController.class_eval do

  def put_on_sale
    @product = Spree::Product.where('permalink like :permalink', :permalink => params[:id]).first
    value = params[:product][:discount_price][:amount]
    enabled = params[:product][:discount_price][:enabled]
    start_date = params[:product][:discount_price][:start_date]
    end_date = params[:product][:discount_price][:end_date]
    affect_all = params[:product][:discount_price][:affect_all]=='1'
    @product.put_on_sale(value, "Spree::Calculator::DollarAmountSalePriceCalculator", affect_all, start_date, end_date, enabled)
    redirect_to discount_price_admin_product_url(@product)
  end

  def discount_price
    @product = Spree::Product.where('permalink like :permalink', :permalink => params[:id]).first
  end

end
