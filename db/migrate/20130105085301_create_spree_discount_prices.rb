class CreateSpreeDiscountPrices < ActiveRecord::Migration
  def change
    create_table :spree_discount_prices do |t|
      t.references :variant
      t.string :discount_type
      t.decimal :amount, :precision => 8, :scale => 2, :null => false, :default => 0.0
      t.timestamps
    end
  end
end
