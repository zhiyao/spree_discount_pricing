class AddVariantIdBackToVariant < ActiveRecord::Migration
  def up
    add_column :spree_discount_prices, :variant_id, :integer
  end

  def down
    remove_column :spree_discount_prices, :variant_id
  end
end
